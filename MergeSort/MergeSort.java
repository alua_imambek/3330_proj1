package project1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.io.*;
public class MergeSort {
	
	public static void main(String[] args) throws IOException{
		
		int num_inversions;			//variable to hold number of inversions
		double elapsedTime = 0;
    
		Integer A[] = new Integer [10000];			//Array to hold values from source1
		Integer B[] = new Integer [10000];			//Array to hold values from source2
		Integer C[] = new Integer [10000];			//Array to hold values from source3
		Integer D[] = new Integer [10000];			//Array to hold values from source4
		Integer E[] = new Integer [10000];			//Array to hold values from source5
		Double w1 = 1.00;
		Double w2 = 1.00;
		Double w3 = 1.00;
		Double w4 = 1.00;
		Double w5 = 1.00;
		Double old_w1, old_w2,old_w3, old_w4, old_w5;
		
		System.out.println("Source1: ");
		populateA(A);
		long startTime = System.nanoTime(); 
		num_inversions = inv_count(A);			//sets num_inversions equal to result of inv_count method
		long endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);	
		
		System.out.println("\n\nSource2: ");
		populateB(B);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(B);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);
		
		System.out.println("\n\nSource3: ");
		populateC(C);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(C);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);			
		
		System.out.println("\n\nSource4: ");
		populateD(D);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(D);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);			
		
		System.out.println("\n\nSource5: ");
		populateE(E);
		startTime = System.nanoTime(); 
		num_inversions = inv_count(E);
		endTime = System.nanoTime(); 
		elapsedTime = elapsedTime + (endTime - startTime);
		System.out.println("Number of inversions: " + num_inversions);
		
		do{
			old_w1 = w1;
			old_w2 = w2;
			old_w3 = w3;
			old_w4 = w4;
			old_w5 = w5;
			
			for(int i =0; i < A.length; i++){
				A[i] = (int) (A[i] * w1);
				B[i] = (int) (B[i] * w2);
				C[i] = (int) (C[i] * w3);
				D[i] = (int) (D[i] * w4);
				E[i] = (int) (E[i] * w5);
			}
			
			Integer sum[] = sum(A,B,C,D,E);
			Integer sortedSum[] = merge_sort(sum);
			HashMap <Integer, Integer> ssum = new HashMap <Integer, Integer> (sortedSum.length);
			for(int i = 0; i < sum.length;i++){
				ssum.put(i, sum[i]);
			}
			ssum = sortHashMapByValues(ssum);
			A = adj(A,ssum);
			B = adj(B,ssum);
			C = adj(C,ssum);
			D = adj(D,ssum);
			E = adj(E,ssum);
			
			double inv_count1 = (double) inv_count(A);
			double inv_count1 = (double) inv_count(A);
			double inv_count1 = (double) inv_count(A);
			double inv_count1 = (double) inv_count(A);

			
			Double r1 =  (1 / (inv_count1 +1));
			Double r2 = (double) (1 / (inv_count(B) +1));
			Double r3 = (double) (1 / (inv_count(C) +1));
			Double r4 = (double) (1 / (inv_count(D) +1));
			Double r5 = (double) (1 / (inv_count(E) +1));

			w1 = (r1/(r1+r2+r3+r4+r5))*5;
			w2 = (r2/(r1+r2+r3+r4+r5))*5;
			w3 = (r3/(r1+r2+r3+r4+r5))*5;
			w4 = (r4/(r1+r2+r3+r4+r5))*5;
			w5 = (r5/(r1+r2+r3+r4+r5))*5;
		}while(w1 != old_w1 && w2 != old_w2 && w3 != old_w3 && w4 != old_w4 && w5 != old_w5);
		
		System.out.println("\nTotal elapsed time: " + elapsedTime/1000000000 + " seconds");
		if(w1 > w2 && w1 > w3 && w1 > w4 && w1 > w5)
			System.out.println("\nMost reliable sourse is Sourse 1");
		else if(w2 > w1 && w2 > w3 && w2 > w4 && w2 > w5)
			System.out.println("\nMost reliable sourse is Sourse 2");
		else if(w3 > w1 && w3 > w2 && w3 > w4 && w3 > w5)
			System.out.println("\nMost reliable sourse is Sourse 3");
		else if(w4 > w1 && w4 > w2 && w4 > w3 && w1 > w5)
			System.out.println("\nMost reliable sourse is Sourse 4");
		else if(w5 > w2 && w5 > w3 && w5 > w4 && w5 > w1)
			System.out.println("\nMost reliable sourse is Sourse 5");
		
	}
	public static Integer [] adj (Integer A[], HashMap<Integer, Integer> passedMap){
		Integer B[] = new Integer [A.length];
		Iterator it = passedMap.entrySet().iterator();
		int i = 0;
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        int j = (int) pair.getKey();
	        B[i] = A[j];
	        i++;
	        it.remove(); // avoids a ConcurrentModificationException
	    }	
		
		return B;
	}
	public static LinkedHashMap<Integer, Integer> sortHashMapByValues(
	    HashMap<Integer, Integer> passedMap) {
	    List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
	    List<Integer> mapValues = new ArrayList<>(passedMap.values());
	    Collections.sort(mapValues);
	    Collections.sort(mapKeys);
	
	    LinkedHashMap<Integer, Integer> sortedMap =
	        new LinkedHashMap<>();
	
	    Iterator<Integer> valueIt = mapValues.iterator();
	    while (valueIt.hasNext()) {
	        Integer val = valueIt.next();
	        Iterator<Integer> keyIt = mapKeys.iterator();
	
	        while (keyIt.hasNext()) {
	            Integer key = keyIt.next();
	            Integer comp1 = passedMap.get(key);
	            Integer comp2 = val;
	
	            if (comp1.equals(comp2)) {
	                keyIt.remove();
	                sortedMap.put(key, val);
	                break;
	            }
	        }
	    }
	    return sortedMap;
	}
	public static Integer[] merge_sort(Integer[] F){
		if(F.length <= 1){			//Base case; if there are 0 or 1 elements, array is sorted
			return F;
		}
		
		int midpoint = F.length/2;			//sets midpoint to the middle value
		Integer[] left = new Integer[midpoint];		//creates an array to hold left-hand values
		Integer[] right;						//creates an array to hold right-hand values
		
		if(F.length%2 == 0){
			right = new Integer[midpoint];		//If even number of elements, array can be divided evenly
		}
		else{
			right = new Integer[midpoint+1];	//If odd number of elements, right side starts 1 element after midpoint
		}
		
		Integer[] result = new Integer[F.length];	//variable to hold the result of the array
		
		for(int i = 0; i < midpoint; i++){	//fills left-hand array
			left[i] = F[i];
			
		}
		
		int x=0;
		for(int j = midpoint; j < F.length; j++){		//fills right-hand array
			if(x<right.length){
			right[x] = F[j];
			x++;
			}
		}
		
		
		left = merge_sort(left);		//recursive call to split array
		right = merge_sort(right);		//recursive call to split array
		
		result = merge(left, right);	//merges left-hand and right-hand sides
		
		
		return result;
		
	}
	public static Integer[] merge(Integer[] left, Integer[] right){
		int lengthResult = left.length + right.length;		//gets length of total list
		Integer[] result = new Integer[lengthResult];				//set result to size of list
		int indexL = 0;										//index to traverse left-hand array
		int indexR = 0;										//index to traverse right-hand array
		int indexRes = 0;
		
		while(indexL < left.length || indexR < right.length){
			if(indexL < left.length && indexR < right.length){		//checks for end of list
				if(left[indexL] <= right[indexR]){					//checks for end of left-hand list
					result [indexRes] = left[indexL];
					indexL++;
					indexRes++;
				}
				
				else{
					result[indexRes] = right[indexR];		//checks for end of right-hand list
					indexR++;
					indexRes++;
				}
			}
			
			else if(indexL < left.length){
				result[indexRes] = left[indexL];
				indexL++;
				indexRes++;
			}
			
			else if(indexR < right.length){
				result[indexRes] = right[indexR];
				indexR++;
				indexRes++;
			}
		}
		return result;
	}
	public static int inv_count(Integer[] F){
		
		int num_inversions = 0;
		  for (int i = 0; i < F.length - 1; i++)	//traverses array
		    for (int j = i+1; j < F.length; j++)	//traverses array 1 element after i
		      if (F[i] > F[j]){						//compares F[i] and F[j] to find inversions
		    	  if(F[i] > 0 && F[j] > 0)     //only for testing purposes
		    		  num_inversions++;
		      }
		
		return num_inversions;
	}
	public static void print(int[] F){				//prints sorted list
		for(int i = 0; i < F.length; i++){
			if(F[i] > 0){
			System.out.print(F[i] + " ");
			}
		}
	}
	public static Integer[] populateA(Integer[] A)throws IOException{		//loads data from Source1 into A
		
		int j = 0;
		File inFile = new File ("C:/java_temp/algorithm/project1/source1_count.txt");
	    Scanner sc = new Scanner (inFile);
	    while (sc.hasNextLine())
	    {
	      String line = sc.nextLine();
	      int i = Integer.parseInt(line);
	      //System.out.println (line);
	      A[j] = i;
	      j++;
	    }
	    sc.close();
		return A;
	}
	public static Integer[] populateB(Integer[] B)throws IOException{		//loads data from Source2 into B
		
		int j = 0;
		File inFile = new File ("C:/java_temp/algorithm/project1/source2_count.txt");
	    Scanner sc = new Scanner (inFile);
	    while (sc.hasNextLine())
	    {
	      String line = sc.nextLine();
	      int i = Integer.parseInt(line);
	      //System.out.println (line);
	      B[j] = i;
	      j++;
	    }
	    sc.close();
		return B;
	}
	public static Integer[] populateC(Integer[] F)throws IOException{		//loads data from Source3 into C
		
		int j = 0;
		File inFile = new File ("C:/java_temp/algorithm/project1/source3_count.txt");
	    Scanner sc = new Scanner (inFile);
	    while (sc.hasNextLine())
	    {
	      String line = sc.nextLine();
	      int i = Integer.parseInt(line);
	      //System.out.println (line);
	      F[j] = i;
	      j++;
	    }
	    sc.close();
		return F;
	}
	public static Integer[] populateD(Integer[] F)throws IOException{		//loads data from Source4 into D
		
		int j = 0;
		File inFile = new File ("C:/java_temp/algorithm/project1/source4_count.txt");
	    Scanner sc = new Scanner (inFile);
	    while (sc.hasNextLine())
	    {
	      String line = sc.nextLine();
	      int i = Integer.parseInt(line);
	      //System.out.println (line);
	      F[j] = i;
	      j++;
	    }
	    sc.close();
		return F;
	}
	public static Integer[] populateE(Integer[] F)throws IOException{		//loads data from Source5 into E
		
		int j = 0;
		File inFile = new File ("C:/java_temp/algorithm/project1/source5_count.txt");
	    Scanner sc = new Scanner (inFile);
	    while (sc.hasNextLine())
	    {
	      String line = sc.nextLine();
	      int i = Integer.parseInt(line);
	      //System.out.println (line);
	      F[j] = i;
	      j++;
	    }
	    sc.close();
		return F;
	}

	public static Integer[] sum(Integer[] A, Integer[] B, Integer[] C, Integer[] D, Integer[] E){
		Integer[] F = new Integer [10000];
		
		for(int i = 0; i < A.length; i++){
			F[i] = A[i] + B[i] + C[i] + D[i] + E[i];
		}
		
		return F;
	}
}